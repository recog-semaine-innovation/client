import React from "react"
import PropTypes from "prop-types"
import SearchIcon from "../icons/SearchIcon"

const SearchBar = ({ onChange, className, label, placeholder }) => (
  <div className={`w-full h-12 relative ${className}`} role="search">
    <input
      onChange={onChange}
      aria-label={label}
      type="text"
      className="pl-4 pr-12 w-full h-full rounded-full shadow-md text-base focus:outline-none focus:shadow-outline"
      placeholder={placeholder}
    />
    <SearchIcon className="absolute top-0 right-2 transform translate-y-1/2 opacity-50" />
  </div>
)

SearchBar.defaultProps = {
  className: "",
}

SearchBar.propTypes = {
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
}

export default SearchBar
