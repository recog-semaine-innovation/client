import React from "react"
import { nanoid } from "nanoid"
import PropTypes from "prop-types"
import AudioPlayer from "./AudioPlayer"

const Recorder = ({ onUpload }) => {
  const [records, setRecords] = React.useState([])
  const [recorder, setRecorder] = React.useState()
  const [recording, setRecording] = React.useState(false)
  const [currentRecord, setCurrentRecord] = React.useState(0)
  const MAX_RECORDS = 3

  const recordAudio = async index => {
    return new Promise(res => {
      navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
        const mediaRecorder = new MediaRecorder(stream)

        const timerProgress = {
          interval: undefined,
          time: 0,
          progress: 0,
          max: 6000,
        }
        const audioChunks = []
        mediaRecorder.addEventListener("dataavailable", event => {
          audioChunks.push(event.data)
        })
        const stopTimer = () => {
          timerProgress.progress = 100
          clearInterval(timerProgress.interval)
          setRecorder()
          setRecording(false)
        }

        const stop = () => {
          mediaRecorder.stop()
          stopTimer()
        }

        const startTimer = () => {
          timerProgress.interval = setInterval(() => {
            if (timerProgress.time < timerProgress.max) {
              timerProgress.time += 100
              timerProgress.progress = +(
                (timerProgress.time * 100) /
                timerProgress.max
              ).toFixed()

              const nextRecords = [...records]
              nextRecords[index] = {
                ...nextRecords[index],
                progress: timerProgress.progress,
              }
              setRecords(nextRecords)
            } else {
              stop()
            }
          }, 100)
        }

        const start = () => {
          mediaRecorder.start()
          startTimer()
        }

        const onStop = () => {
          return new Promise(onStopResolver => {
            mediaRecorder.addEventListener("stop", () => {
              const audioBlob = new Blob(audioChunks, { type: "audio/mpeg" })
              const audioUrl = URL.createObjectURL(audioBlob)
              const audio = new Audio(audioUrl)

              const newRecord = {
                audio,
                audioBlob,
                valid: true,
              }
              onStopResolver(newRecord)
            })
          })
        }

        res({ start, stop, onStop })
      })
    })
  }

  const onRecord = async () => {
    if (currentRecord < MAX_RECORDS) {
      setRecorder(await recordAudio(currentRecord))
    }
  }

  const stopRecorder = () => {
    setRecording(false)
    recorder.stop()
  }

  const onRemoveRecord = recordId => {
    const nextRecords = [
      ...records.filter(({ id }) => id !== recordId),
      {
        id: recordId,
        valid: false,
      },
    ]
    setRecords(nextRecords)
    setCurrentRecord(currentRecord - 1)
  }

  React.useEffect(() => {
    const newRecords = []
    for (let i = 0; i < MAX_RECORDS; i++) {
      const newRecord = {
        valid: false,
        progress: 0,
        id: nanoid(),
      }
      newRecords.push(newRecord)
    }
    setRecords([...records, ...newRecords])
  }, [])

  React.useEffect(() => {
    if (recorder) {
      const startRecorder = async () => {
        setRecording(true)
        recorder.start()

        const newRecord = await recorder.onStop()
        const newRecords = [...records]
        newRecords[currentRecord] = {
          ...newRecords[currentRecord],
          ...newRecord,
        }
        setRecords(newRecords)
        setCurrentRecord(currentRecord + 1)
      }
      startRecorder()
    }
  }, [recorder])

  return (
    <main className="h-screen flex flex-col md:flex-row">
      <div
        className={`flex flex-col items-center justify-center transition-all ease-linear duration-75 w-full relative ${
          currentRecord < 3 ? "h-full" : "h-0 md:h-full"
        }`}
      >
        <h1 className="text-xl md:text-4xl font-bold md:absolute top-2 md:top-12 text-center max-w-full mb-4">
          {currentRecord < 3
            ? `Il vous reste ${3 - currentRecord} enregistrement à effectuer`
            : "Vous pouvez téléverser vos enregistrements"}
        </h1>
        <button
          disabled={currentRecord >= 3}
          onClick={!recording ? () => onRecord() : () => stopRecorder()}
          type="button"
          className="border-8 border-purple-600 rounded-full w-64 h-64 text-2xl font-bold text-purple-600 
          focus:outline-none focus:shadow-outline hover:bg-purple-100 active:bg-purple-300  disabled:opacity-50 disabled:cursor-not-allowed
          md:text-4xl md:w-72 md:h-72"
          aria-label={
            !recording ? "Lancer l'enregistrement" : "Arrêter l'enregistrement"
          }
        >
          {!recording ? "Enregistrer" : "stop"}
        </button>
      </div>
      <div
        className={`bg-purple-600 p-4 md:p-8 pb-0 transition-all ease-linear duration-75 relative 
        md:min-w-2/5 md:flex md:flex-col md:justify-center ${
          currentRecord < 3 ? "flex-shrink-0" : "h-full"
        }`}
      >
        {records.map(({ valid, audio, id, progress }, index) => (
          <AudioPlayer
            audio={audio}
            valid={valid}
            onRemoveRecord={() => onRemoveRecord(id)}
            progress={progress}
            id={index}
            key={id}
          />
        ))}

        <button
          onClick={() => onUpload(records)}
          type="button"
          className={`bg-purple-800 w-full h-10 focus:outline-none focus:shadow-outline shadow-md rounded-full text-white ${
            currentRecord < MAX_RECORDS ? "hidden" : "block"
          }`}
        >
          Envoyer les enregistrements
        </button>
      </div>
    </main>
  )
}

Recorder.propTypes = {
  onUpload: PropTypes.func.isRequired,
}

export default Recorder
