import React from "react"
import {
  NavLink,
  Switch,
  Route,
  useRouteMatch,
  Redirect,
} from "react-router-dom"
import RecordsSearch from "./RecordsSearch"
import ManagePathologies from "./ManagePathologies"

const AdminDashboard = () => {
  const { path } = useRouteMatch()

  return (
    <div className="flex flex-col items-stretch h-screen">
      <nav className="p-4 md:p-12 pt-8 pb-8 max-w-lg">
        <NavLink
          aria-label="Gérer les enregistrements"
          className="w-1/2 text-purple-600 rounded-tl-full rounded-bl-full focus:z-10 relative text-center 
          border-4 border-purple-600 p-4 pt-2 pb-2 inline-block text-lg font-semibold focus:outline-none focus:shadow-outline"
          to="/dashboard/records"
          activeClassName="bg-purple-600 text-white-imp"
        >
          enregistrements
        </NavLink>
        <NavLink
          aria-label="Gérer les pathologies"
          className="w-1/2 text-purple-600 rounded-tr-full rounded-br-full focus:z-10 relative text-center
          border-4 border-purple-600 p-4 pt-2 pb-2 inline-block text-lg font-semibold focus:outline-none focus:shadow-outline"
          to="/dashboard/pathologies"
          activeClassName="bg-purple-600 text-white-imp"
        >
          pathologies
        </NavLink>
      </nav>
      <main className="h-full bg-purple-600 overflow-y-scroll">
        <Switch>
          <Route exact path={`${path}/records`}>
            <RecordsSearch />
          </Route>
          <Route exact path={`${path}/pathologies`}>
            <ManagePathologies />
          </Route>
          <Redirect exact path={path} to={`${path}/records`} />
        </Switch>
      </main>
    </div>
  )
}
AdminDashboard.propTypes = {}
export default AdminDashboard
