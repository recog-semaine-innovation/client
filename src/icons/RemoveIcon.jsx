import React from "react"
import PropTypes from "prop-types"

const RemoveIcon = ({ width, height, className, label }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    className={className}
    aria-label={label}
  >
    <path d="M3 6h18M19 6v14a2 2 0 01-2 2H7a2 2 0 01-2-2V6m3 0V4a2 2 0 012-2h4a2 2 0 012 2v2" />
  </svg>
)

RemoveIcon.defaultProps = {
  width: 24,
  height: 24,
  className: "",
  label: "Remove icon",
}

RemoveIcon.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  className: PropTypes.string,
  label: PropTypes.string,
}

export default RemoveIcon
