import React from "react"
import PropTypes from "prop-types"

const StopIcon = ({ width, height, className, label }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
    className={className}
    aria-label={label}
  >
    <rect x="3" y="3" width="18" height="18" rx="2" ry="2" />
  </svg>
)

StopIcon.defaultProps = {
  width: 24,
  height: 24,
  className: "",
  label: "Stop icon",
}

StopIcon.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  className: PropTypes.string,
  label: PropTypes.string,
}

export default StopIcon
