import React from "react"
import "./App.css"
import { Switch, Route } from "react-router-dom"
import Landing from "./views/Landing"
import Record from "./views/Record"
import AdminDashboard from "./views/AdminDashboard"

function App() {
  return (
    <Switch>
      <Route exact path="/">
        <Landing />
      </Route>
      <Route path="/record">
        <Record />
      </Route>
      <Route path="/dashboard">
        <AdminDashboard />
      </Route>
    </Switch>
  )
}

export default App
