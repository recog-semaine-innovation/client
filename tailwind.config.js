module.exports = {
  purge: [],
  theme: {
    extend: {
      borderRadius: {
        xlg: "1rem",
      },
      inset: {
        "-2": "-1rem",
        "2": "1rem",
        "12": "3rem",
        "1/2": "50%",
      },
      width: {
        "72": "18rem",
      },
      height: {
        "72": "18rem",
        "1/2": "50%",
        "3/5": "60%",
        "2/5": "40%",
      },
      minWidth: {
        "15": "15rem",
        "2/5": "40%",
      },
      colors: {
        "white-imp": "#fff !important",
      },
    },
  },
  variants: {
    backgroundColor: ["responsive", "hover", "focus", "active", "disabled"],
    zIndex: ["responsive", "hover", "focus", "active"],
    opacity: ["responsive", "hover", "focus", "active", "disabled"],
    cursor: ["responsive", "hover", "focus", "active", "disabled"],
    textColor: [
      "responsive",
      "hover",
      "focus",
      "active",
      "disabled",
      "visited",
    ],
    borderColor: [
      "responsive",
      "hover",
      "focus",
      "active",
      "disabled",
      "visited",
    ],
  },
  plugins: [],
}
